const mysql = require('mysql');

const { promisify } = require('util');

const {database} = require('./keys');
 


//especie de hilos si tenemos fallosc
const pool = mysql.createPool(database);

pool.getConnection((err, connection) =>{
    if (err){
        if (err.code === 'PROTOCOL_CONNECTION?LOST'){
            console.error('LA conexion de la base de datos fue cerrada');

        }
        if(err.code === 'ER_CON_COUNT_ERROR'){
            console.error('la base de datos has to many conections');
        }
        if(err.code === 'ECONNREFUSED'){
            console.error('la conexion fue rechazada');
        }
    }


    if(connection) connection.release();
    console.log('La base esta conectada');
    return;
});
//convertiendo promesas
pool.query = promisify(pool.query); //promociona las consulttas
module.exports = pool;

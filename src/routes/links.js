const express = require('express');
const router = express.Router();

const pool = require('../database');//hace conexion a la base de datos


router.get('/add',(req,res) => {
    res.render('links/add');
});

router.post('/add',async (req, res) => {
   const{distancia,altitud,longitud,fecha,hora,nivel_almacenamiento,material_desecho,estado} = req.body;
   const newLink = {
      distancia,
      altitud,
      longitud,
      fecha,
      hora,
      nivel_almacenamiento,
      material_desecho,
      estado

   };
   
    await pool.query('INSERT INTO vehiculo set ?',[newLink]);
    res.redirect('/links');
});

router.get('/',async (req, res) => {
    const links = await pool.query('SELECT*FROM vehiculo');
    console.log(links);
    res.render('links/list',{links});
 });

 router.get('/delete/:id',async(req, res) => {
     const {id} = req.params;
     await pool.query('DELETE FROM vehiculo WHERE ID =?',[id]);
     //msj cambiado
     req.flash('success', 'Datos eliminado');
     res.redirect('/links');
 });
 router.get('/edit/:id',async(req, res) => {
    const {id} = req.params;
    const links = await pool.query('SELECT * FROM vehiculo WHERE id=?',[id]);
    res.render('links/edit', {link: links[0]});
});

router.post('/edit/:id', async (req,res) => {
    const {id} = req.params;
    const {distancia,altitud,longitud,fecha,hora,nivel_almacenamiento,material_desecho,estado} = req.body;
    const newLink = {
        distancia,
        altitud,
        longitud,
        fecha,
        hora,
        nivel_almacenamiento,
        material_desecho,
        estado
  
     };
     console.log(newLink);
    await pool.query('UPDATE vehiculo set ? WHERE id = ?', [newLink,id]);
    //esto cambie
    req.flash('success','actualizado correctamente');
    
    res.redirect('/links');
});
module.exports = router;
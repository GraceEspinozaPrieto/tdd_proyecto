//metodo de autientificacion

const passport = require('passport');

const LocalStrategy = require('passport-local').Strategy;

const pool = require('../database');
const helpers = require('../lib/helpers');

//login
passport.use('local.signin', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async(req, username, password, done) =>{
   const rows = await pool.query('SELECT * FROM logins WHERE username = ?', [username]);
    if(rows.length > 0){
        const user = rows[0];
        const validPassword = await helpers.matchPassword(password, user.password);
        if(validPassword){
            done(null, user, req.flash('success','Bienvenido ' + user.username));

        } else{
            done(null, false, req.flash('message','Contrasena incorrecta'));
        }
    }else{
        return done(null, false, req.flash('message','El nombre de usuario no existe'));
    }
}));



passport.use('local.signup', new LocalStrategy  ({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) => {
    //console.log(req.body);
    const { nombres, apellidos,correo } =  req.body;
    //aqui va la mas variables
    const newUser = {
        nombres,
        apellidos,
        username,
        password,
        correo
    };
    newUser.password = await helpers.encryptPassword(password);
    const result = await pool.query('INSERT INTO logins SET ?', [newUser]);
    console.log(result);
    newUser.id = result.insertId;
    return done(null, newUser);
}));

//terealizar el usuario
passport.serializeUser((user, done) => {
    done(null, user.id);
});
//desteri....
passport.deserializeUser(async (id, done) => {
    const rows = await pool.query('SELECT * FROM logins Where id = ?', [id]);
    done(null, rows[0]);
});
CREATE DATABASE proyecto_g5;
USE proyecto_g5;

CREATE TABLE logins(
    id INT(11) NOT NULL,
    username VARCHAR(16) NOT NULL,
    password VARCHAR(60) NOT NULL,
    nombres VARCHAR (50) NOT NULL,
    apellidos VARCHAR (50) NOT NULL,
    correo VARCHAR(60) NOT NULL,
    created_at timestamp NOT NULL DEFAULT current_timestamp
);

ALTER TABLE logins
    ADD PRIMARY KEY (id);

ALTER TABLE logins
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 1;

DESCRIBE logins;

--  TABLE
CREATE TABLE vehiculo(
    id INT (11) NOT NULL,
    distancia VARCHAR (150) NOT NULL,
    altitud VARCHAR  (150) NOT NULL,
    longitud VARCHAR (150) NOT NULL,
    fecha DATE  NOT NULL,
    hora VARCHAR (150) NOT NULL,
    nivel_almacenamiento VARCHAR (150) NOT NULL,
    material_desecho VARCHAR  (150) NOT NULL,
    estado VARCHAR (150) NOT NULL,
    logins_id INT (11),
    created_at timestamp NOT NULL DEFAULT current_timestamp,
    CONSTRAINT fk_logins FOREIGN KEY (logins_id) REFERENCES logins(id)
);

ALTER TABLE vehiculo 
    ADD PRIMARY KEY (id);
ALTER TABLE vehiculo
    MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT =1;
DESCRIBE vehiculo;

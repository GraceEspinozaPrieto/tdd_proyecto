import request from 'supertest'
import sinon from 'sinon'
import proxyquire from 'proxyquire'
import test from 'ava'

const version = '/v1'

const sandbox = sinon.createSandbox()

let app = null
let userStub = null

test.before((t) => {
  userStub = {
    save () {}
  }
  userStub['@global'] = true

  sandbox.stub(userStub, 'save')
  userStub.save.withArgs({}).returns(true)

  app = proxyquire('../app', {
    './models/userModel': userStub
  })
})

test.after(() => {
  sandbox.restore()
})


test.serial.cb('save valid user data', (t) => {
 
  const user = { name: 'Allison', email: 'allison-@hotmail.com' }


  request(app)
    .post(`${version}/users`)
    .send(user)
    .expect('Content-Type', /json/)
    .expect(201)
    .end((err, res) => {
      t.falsy(err, 'should not error')
      t.end()

      
      sandbox.assert.calledOnce(userStub.save)
      sandbox.assert.calledWith(userStub.save, user)
    })
})



test.serial.cb('Validate user data', (t) => {
 
  const user = { name: 'Allison', email: 'allison-@hotmail.com' }

 
  request(app)
    .post(`${version}/users`)
    .send(user)
    .expect('Content-Type', /json/)
    .expect(400)
    .end((err, res) => {
      t.falsy(err, 'should not error')
     
      t.deepEqual(res.body, {
        errors: {
          email: 'the email must be like "allison-@hotmail.com"'
        }
      })

      t.end()
    })
})
